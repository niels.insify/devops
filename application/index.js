const express = require('express');
const {getGrid, render} = require("./app");

const app = express()
const port = 3000

app.get('/', (req, res)=> {
    const text = (req.query['say'] || 'hello').toLowerCase().replace(/\s+/g, ' ');
    try {
        const grid = getGrid(text);
        const html = render({grid});
        res.send(html);
    }catch(e) {
        res.status(400).send(e.message);
    }
});

app.listen(port, () => {
    console.log(`Running on :${port}`);
});
